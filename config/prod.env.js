// SPDX-FileCopyrightText: 2017-2019 PanJiaChen <https://github.com/PanJiaChen/vue-element-admin>
// SPDX-License-Identifier: MIT
//
// SPDX-License-Identifier: AGPL-3.0-only
// SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>

module.exports = {
  NODE_ENV: '"production"',
  ENV_CONFIG: '"prod"',
  BASE_API: '"https://api-prod"',
  DISABLED_FEATURES: '[""]',
  ASSETS_PUBLIC_PATH: '/pleroma/admin/'
}
