// SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>
// SPDX-License-Identifier: AGPL-3.0-only

export async function fetchLog(authHost, token, params, page = 1) {
  return Promise.resolve()
}

export async function fetchAdmins(authHost, token) {
  return Promise.resolve()
}

export async function fetchModerators(authHost, token) {
  return Promise.resolve()
}
