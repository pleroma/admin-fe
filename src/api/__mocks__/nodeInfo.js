// SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>
// SPDX-License-Identifier: AGPL-3.0-only

export async function getNodeInfo(authHost) {
  const data = {
    metadata: {
      mailerEnabled: true
    }
  }

  return Promise.resolve({ data })
}
