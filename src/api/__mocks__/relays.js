// SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>
// SPDX-License-Identifier: AGPL-3.0-only

export async function fetchRelays(authHost, token) {
  return Promise.resolve()
}

export async function addRelay(relay_url, authHost, token) {
  return Promise.resolve()
}

export async function deleteRelay(relay_url, authHost, token) {
  return Promise.resolve()
}
