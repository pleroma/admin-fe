// SPDX-FileCopyrightText: 2017-2019 PanJiaChen <https://github.com/PanJiaChen/vue-element-admin>
// SPDX-License-Identifier: MIT
//
// SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>
// SPDX-License-Identifier: AGPL-3.0-only

import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css'// progress bar style
import { getToken } from '@/utils/auth' // getToken from cookie

NProgress.configure({ showSpinner: false })// NProgress Configuration

// permission judge function
function hasPermission(roles, permissionRoles) {
  if (roles.indexOf('admin') >= 0) return true // admin permission passed directly
  if (!permissionRoles) return true
  return roles.some(role => permissionRoles.indexOf(role) >= 0)
}

function isPrivileged(route, privileges) {
  if (!route.required_privileges) {
    return true
  }

  // We check for all the required privileges if the user has it
  // If there's at least one privilege missing, the user isn't privileged so we return false
  // If the logged in user has all required privileges, we return true
  return route.required_privileges.map(required_privilege => privileges.indexOf(required_privilege)).indexOf(-1) === -1
}

function findFirstUnhiddenPath(addRouters) {
  const unhiddenRoute = addRouters.find((route) => !route.hidden)
  if (unhiddenRoute) {
    return unhiddenRoute.path + '/index'
  }
  return '/401'
}

const whiteList = ['/login', '/auth-redirect', '/login-pleroma']// no redirect whitelist

export const beforeEachRoute = (to, from, next) => {
  NProgress.start() // start progress bar
  if (getToken()) { // determine if there has token
    /* has token*/
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done() // if current page is dashboard will not trigger	afterEach hook, so manually handle it
    } else {
      if (store.getters.roles.length === 0 && store.getters.privileges.length === 0) {
        store.dispatch('GetUserInfo').then(res => {
          const roles = store.getters.roles
          const privileges = store.getters.privileges

          store.dispatch('GenerateRoutes', { roles }).then(() => {
            const addRouters = store.getters.addRouters
            addRouters.forEach(route => {
              route.hidden = route.hidden || !isPrivileged(route, privileges)
              if (route.path === '') {
                route.redirect = findFirstUnhiddenPath(addRouters)
              }
              router.addRoute(route)
            })
            next({ ...to, replace: true })
          })
        }).catch((err) => {
          store.dispatch('FedLogOut').then(() => {
            Message({
              dangerouslyUseHTMLString: true,
              message: err,
              type: 'error',
              duration: 10 * 1000
            })
            next({ path: '/' })
          })
        })
      } else {
        if (hasPermission(store.getters.roles, to.meta.roles)) {
          next()
        } else {
          next({ path: '/401', replace: true, query: { noGoBack: true }})
        }
      }
    }
  } else {
    /* has no token*/
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next(`/login?redirect=${to.path}`)
      NProgress.done() // if current page is login will not trigger afterEach hook, so manually handle it
    }
  }
}
router.beforeEach(beforeEachRoute)

router.afterEach(() => {
  NProgress.done() // finish progress bar
})
