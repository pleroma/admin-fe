import Vuex from 'vuex'
import { mount, createLocalVue, config } from '@vue/test-utils'
import flushPromises from 'flush-promises'
import Element from 'element-ui'
import Statuses from '@/views/statuses/index'
import {
  storeNoPrivilegesNoRoles,
  storeWithPrivilegesMessagesDeleteNoRoles
} from './store.conf'
import { cloneDeep } from 'lodash'

config.mocks["$t"] = (t) => t
config.stubs.transition = false

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Element)

jest.mock('@/api/app')
jest.mock('@/api/nodeInfo')
jest.mock('@/api/peers')
jest.mock('@/api/settings')
jest.mock('@/api/status')

describe('Statuses', () => {
  it('Allows to change scope when privileged', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeWithPrivilegesMessagesDeleteNoRoles))
    const wrapper = mount(Statuses, {
      store: store,
      localVue
    })
    await flushPromises()

    store.dispatch('HandleFilterChange', 'heaven.com')
    wrapper.vm.handleFilterChange()
    await flushPromises()

    const menu = wrapper.find('.status-actions')
    const menu_items = menu.findAll('.el-dropdown-menu__item')
    const menu_items_text = menu_items.wrappers.map(menu_item => menu_item.text()).sort()

    expect(menu.isVisible()).toEqual(true)
    expect(menu_items_text).toEqual([
      'reports.addSensitive',
      'reports.deleteStatus',
      'reports.private',
      'reports.public'
    ])

    done()
  })

  it('Allows to change scope when not privileged', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeNoPrivilegesNoRoles))
    const wrapper = mount(Statuses, {
      store: store,
      localVue
    })
    await flushPromises()

    store.dispatch('HandleFilterChange', 'heaven.com')
    wrapper.vm.handleFilterChange()
    await flushPromises()

    const menu = wrapper.find('.status-actions')

    expect(menu.exists()).toEqual(false)

    done()
  })
})
