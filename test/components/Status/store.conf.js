import app from '@/store/modules/app'
import peers from '@/store/modules/peers'
import user from '@/store/modules/user'
import users from '@/store/modules/users'
import settings from '@/store/modules/settings'
import status from '@/store/modules/status'
import getters from '@/store/getters'

export const storeNoPrivilegesNoRoles = {
  modules: {
    app,
    peers,
    settings,
    status,
    user: {
      ...user,
      state: {
        ...user.state,
        authHost: 'localhost:4000',
        roles: [],
        privileges: []
      }
    },
    users
  },
  getters
}

export const storeWithPrivilegesMessagesDeleteNoRoles = {
  modules: {
    app,
    peers,
    settings,
    status,
    user: {
      ...user,
      state: {
        ...user.state,
        authHost: 'localhost:4000',
        roles: [],
        privileges: ['messages_delete']
      }
    },
    users
  },
  getters
}
