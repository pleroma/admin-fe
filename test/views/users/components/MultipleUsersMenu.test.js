import Vuex from 'vuex'
import { mount, createLocalVue, config, RouterLinkStub } from '@vue/test-utils'
import flushPromises from 'flush-promises'
import Element from 'element-ui'
import Users from '@/views/users/index'
import {
  storeNoPrivilegesNoRoles,
  storeWithTagPolicyNoPrivilegesRolesAdmin,
  storeWithPrivilegesUsersManageInvitesNoRoles,
  storeWithPrivilegesUsersDeleteNoRoles,
  storeWithPrivilegesUsersManageActivationStateNoRoles,
  storeWithTagPolicyPrivilegesUsersManageTagsNoRoles,
  storeWithTagPolicyPrivilegesUsersManageTagsRolesAdmin
} from './store.conf'
import { cloneDeep } from 'lodash'

config.mocks["$t"] = (key) => key

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Element)

jest.mock('@/api/app')
jest.mock('@/api/nodeInfo')
jest.mock('@/api/users')
jest.mock('@/api/settings')

describe('The Multiple Users Moderation Menu', () => {
  it('doesnt show for someone with no privileges and no roles', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeNoPrivilegesNoRoles))
    const wrapper = mount(Users, {
      store,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    await flushPromises()

    expect(wrapper.find('.multiple-users-menu').exists()).toEqual(false)
    done()
  })

  it('shows for someone with admin role and shows proper entries', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeWithTagPolicyNoPrivilegesRolesAdmin))
    const wrapper = mount(Users, {
      store,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    await flushPromises()
    await wrapper.find('.el-table').findAll('input').at(1).setChecked()
    const menu = wrapper.find('.multiple-users-menu')
    const menu_items = menu.findAll('.el-dropdown-menu__item')
    const menu_items_text = menu_items.wrappers.map(menu_item => menu_item.text()).sort()

    expect(menu.isVisible()).toEqual(true)
    expect(menu_items_text).toEqual([
      'users.confirmAccounts',
      'users.grantAdmin',
      'users.grantModerator',
      'users.requirePasswordReset',
      'users.resendConfirmation',
      'users.revokeAdmin',
      'users.revokeModerator'
    ])

    store.state.users.mrfPolicies = []
    await flushPromises()

    expect(menu_items.length).toEqual(7)
    done()
  })

  it('shows for someone with users_manage_invites privilege and shows proper entries', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeWithPrivilegesUsersManageInvitesNoRoles))
    const wrapper = mount(Users, {
      store,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    await flushPromises()
    await wrapper.find('.el-table').findAll('input').at(1).setChecked()
    const menu = wrapper.find('.multiple-users-menu')
    const menu_items = menu.findAll('.el-dropdown-menu__item')
    const menu_items_text = menu_items.wrappers.map(menu_item => menu_item.text())

    expect(menu.isVisible()).toEqual(true)
    expect(menu_items_text).toEqual(['users.approveAccounts'])
    done()
  })

  it('shows for someone with users_delete privilege and shows proper entries', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeWithPrivilegesUsersDeleteNoRoles))
    const wrapper = mount(Users, {
      store,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    await flushPromises()
    await wrapper.find('.el-table').findAll('input').at(1).setChecked()
    const menu = wrapper.find('.multiple-users-menu')
    const menu_items = menu.findAll('.el-dropdown-menu__item')
    const menu_items_text = menu_items.wrappers.map(menu_item => menu_item.text()).sort()

    expect(menu.isVisible()).toEqual(true)
    expect(menu_items_text).toEqual(['users.deleteAccounts', 'users.rejectAccounts'])
    done()
  })

  it('shows for someone with users_manage_activation_state privilege and shows proper entries', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeWithPrivilegesUsersManageActivationStateNoRoles))
    const wrapper = mount(Users, {
      store,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    await flushPromises()
    await wrapper.find('.el-table').findAll('input').at(1).setChecked()
    const menu = wrapper.find('.multiple-users-menu')
    const menu_items = menu.findAll('.el-dropdown-menu__item')
    const menu_items_text = menu_items.wrappers.map(menu_item => menu_item.text()).sort()

    expect(menu.isVisible()).toEqual(true)
    expect(menu_items_text).toEqual(['users.activateAccounts', 'users.deactivateAccounts'])
    done()
  })

  it('shows for someone with users_manage_tags privilege and shows proper entries for non-admin/admin and with/without TagPolicy enabled', async (done) => {
    const store_no_admin = new Vuex.Store(cloneDeep(storeWithTagPolicyPrivilegesUsersManageTagsNoRoles))
    const wrapper_no_admin = mount(Users, {
      store: store_no_admin,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    const store_admin = new Vuex.Store(cloneDeep(storeWithTagPolicyPrivilegesUsersManageTagsRolesAdmin))
    const wrapper_admin = mount(Users, {
      store: store_admin,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })

    await flushPromises()
    await wrapper_no_admin.find('.el-table').findAll('input').at(1).setChecked()
    await wrapper_admin.find('.el-table').findAll('input').at(1).setChecked()
    const menu_no_admin = wrapper_no_admin.find('.multiple-users-menu')
    const menu_admin = wrapper_admin.find('.multiple-users-menu')
    const menu_no_admin_items = menu_no_admin.findAll('.el-dropdown-menu__item')
    const menu_admin_items = menu_admin.findAll('.el-dropdown-menu__item')
    const filter = menu_item => { return menu_item.find('.tag-container .tag-text').exists() && menu_item.find('.tag-container .tag-text').text() }
    const menu_no_admin_items_text = menu_no_admin_items.wrappers.map(filter).sort()
    const menu_admin_items_text = menu_admin_items.wrappers.map(filter)

    expect(menu_no_admin.isVisible()).toEqual(true)
    expect(menu_no_admin_items_text).toEqual([
      'users.disableAnySubscriptionForMultiple',
      'users.disableRemoteSubscriptionForMultiple',
      'users.forceNsfw',
      'users.forceUnlisted',
      'users.sandbox',
      'users.stripMedia'
    ])

    expect(menu_admin.isVisible()).toEqual(true)
    expect(menu_admin_items_text.includes('users.forceNsfw')).toBe(true)
    expect(menu_admin_items_text.includes('users.stripMedia')).toBe(true)
    expect(menu_admin_items_text.includes('users.forceUnlisted')).toBe(true)
    expect(menu_admin_items_text.includes('users.sandbox')).toBe(true)
    expect(menu_admin_items_text.includes('users.disableRemoteSubscriptionForMultiple')).toBe(true)
    expect(menu_admin_items_text.includes('users.disableAnySubscriptionForMultiple')).toBe(true)

    store_no_admin.state.users.mrfPolicies = []
    store_admin.state.users.mrfPolicies = []
    await flushPromises()

    // Only an admin with users_manage_tags privilege can enable TagPolicy from the menu
    expect(menu_no_admin.findAll('.el-dropdown-menu__item').length).toEqual(0)
    expect(menu_admin.findAll('.el-dropdown-menu__item').wrappers.map(wrapper => wrapper.text()).includes('users.enableTagPolicy')).toBe(true)
    done()
  })
})
