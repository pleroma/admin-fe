import app from '@/store/modules/app'
import settings from '@/store/modules/settings'
import user from '@/store/modules/user'
import userProfile from '@/store/modules/userProfile'
import users from '@/store/modules/users'
import getters from '@/store/getters'

export const storeNoPrivilegesNoRoles = {
  modules: {
    app,
    settings,
    user: {
      ...user,
      state: {
        ...user.state,
        roles: [],
        privileges: []
      }
    },
    userProfile,
    users
  },
  getters
}

export const storeWithTagPolicyNoPrivilegesRolesAdmin = {
  modules: {
    app,
    settings,
    user: {
      ...user,
      state: {
        ...user.state,
        roles: ['admin'],
        privileges: []
      }
    },
    userProfile,
    users: { ...users, state: { ...users.state, mrfPolicies: ['Pleroma.Web.ActivityPub.MRF.TagPolicy'] }}
  },
  getters
}

export const storeWithPrivilegesUsersManageInvitesNoRoles = {
  modules: {
    app,
    settings,
    user: {
      ...user,
      state: {
        ...user.state,
        roles: [],
        privileges: ['users_manage_invites']
      }
    },
    userProfile,
    users
  },
  getters
}

export const storeWithPrivilegesUsersDeleteNoRoles = {
  modules: {
    app,
    settings,
    user: {
      ...user,
      state: {
        ...user.state,
        roles: [],
        privileges: ['users_delete']
      }
    },
    userProfile,
    users
  },
  getters
}

export const storeWithPrivilegesUsersManageActivationStateNoRoles = {
  modules: {
    app,
    settings,
    user: {
      ...user,
      state: {
        ...user.state,
        roles: [],
        privileges: ['users_manage_activation_state']
      }
    },
    userProfile,
    users
  },
  getters
}

export const storeWithTagPolicyPrivilegesUsersManageTagsNoRoles = {
  modules: {
    app,
    settings,
    user: {
      ...user,
      state: {
        ...user.state,
        roles: [],
        privileges: ['users_manage_tags']
      }
    },
    userProfile,
    users: { ...users, state: { ...users.state, mrfPolicies: ['Pleroma.Web.ActivityPub.MRF.TagPolicy'] }}
  },
  getters
}

export const storeWithTagPolicyPrivilegesUsersManageTagsRolesAdmin = {
  modules: {
    app,
    settings,
    user: {
      ...user,
      state: {
        ...user.state,
        roles: ['admin'],
        privileges: ['users_manage_tags']
      }
    },
    userProfile,
    users: { ...users, state: { ...users.state, mrfPolicies: ['Pleroma.Web.ActivityPub.MRF.TagPolicy'] }}
  },
  getters
}

export const storeWithPrivilegesUsersManageCredentialsNoRoles = {
  modules: {
    app,
    settings,
    user: {
      ...user,
      state: {
        ...user.state,
        roles: [],
        privileges: ['users_manage_credentials']
      }
    },
    userProfile,
    users
  },
  getters
}
