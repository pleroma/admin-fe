import Vuex from 'vuex'
import { mount, createLocalVue, config, RouterLinkStub } from '@vue/test-utils'
import flushPromises from 'flush-promises'
import Element from 'element-ui'
import Users from '@/views/users/index'
import {
  storeNoPrivilegesNoRoles,
  storeWithTagPolicyNoPrivilegesRolesAdmin,
  storeWithPrivilegesUsersManageActivationStateNoRoles,
  storeWithPrivilegesUsersDeleteNoRoles,
  storeWithTagPolicyPrivilegesUsersManageTagsNoRoles,
  storeWithTagPolicyPrivilegesUsersManageTagsRolesAdmin,
  storeWithPrivilegesUsersManageCredentialsNoRoles
} from './store.conf'
import { cloneDeep } from 'lodash'

config.mocks["$t"] = (key) => key

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Element)

jest.mock('@/api/app')
jest.mock('@/api/nodeInfo')
jest.mock('@/api/users')
jest.mock('@/api/settings')

describe('The Multiple Users Moderation Menu', () => {
  it('doesnt show for someone with no privileges and no roles', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeNoPrivilegesNoRoles))
    const wrapper = mount(Users, {
      store,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    await flushPromises()

    expect(wrapper.find('.moderation-dropdown-menu').exists()).toEqual(false)
    done()
  })

  it('shows for someone with admin role and shows proper entries', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeWithTagPolicyNoPrivilegesRolesAdmin))
    const wrapper = mount(Users, {
      store,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    await flushPromises()
    const menu = wrapper.findAll('.moderation-dropdown-menu').at(0)
    const menu_items = menu.findAll('.el-dropdown-menu__item')
    const menu_items_text = menu_items.wrappers.map(menu_item => menu_item.text()).sort()

    expect(menu.isVisible()).toEqual(true)
    expect(menu_items_text).toEqual([
      'users.bot users.person',
      'users.disableMfa',
      'users.grantModerator',
      'users.requirePasswordReset',
      'users.revokeAdmin',
    ])

    store.state.users.mrfPolicies = []
    await flushPromises()

    expect(menu_items.length).toEqual(menu_items_text.length)
    done()
  })

  it('shows for someone with users_manage_activation_state privilege and shows proper entries', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeWithPrivilegesUsersManageActivationStateNoRoles))
    const wrapper = mount(Users, {
      store,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    await flushPromises()
    const menu = wrapper.findAll('.moderation-dropdown-menu').at(0)
    const menu_items = menu.findAll('.el-dropdown-menu__item')
    const menu_items_text = menu_items.wrappers.map(menu_item => menu_item.text()).sort()

    expect(menu.isVisible()).toEqual(true)
    expect(menu_items_text).toEqual(['users.deactivateAccount'])

    done()
  })

  it('shows for someone with users_delete privilege and shows proper entries', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeWithPrivilegesUsersDeleteNoRoles))
    const wrapper = mount(Users, {
      store,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    await flushPromises()
    const menu = wrapper.findAll('.moderation-dropdown-menu').at(0)
    const menu_items = menu.findAll('.el-dropdown-menu__item')
    const menu_items_text = menu_items.wrappers.map(menu_item => menu_item.text()).sort()

    expect(menu.isVisible()).toEqual(true)
    expect(menu_items_text).toEqual(['users.deleteAccount'])

    done()
  })

  it('shows for someone with users_manage_tags privilege and shows proper entries depending on wether tagpolicy is set', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeWithTagPolicyPrivilegesUsersManageTagsNoRoles))
    const wrapper = mount(Users, {
      store,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    await flushPromises()
    const menu = wrapper.findAll('.moderation-dropdown-menu').at(0)
    const menu_items = menu.findAll('.el-dropdown-menu__item')
    const menu_items_text = menu_items.wrappers.map(menu_item => menu_item.text()).sort()

    expect(menu.isVisible()).toEqual(true)
    expect(menu_items_text).toEqual([
      'users.disableAnySubscription',
      'users.disableRemoteSubscription',
      'users.forceNsfw',
      'users.forceUnlisted',
      'users.sandbox',
      'users.stripMedia'
    ])

    store.state.users.mrfPolicies = []
    await flushPromises()

    expect(menu.findAll('.el-dropdown-menu__item').length).toEqual(0)

    done()
  })

  it('shows enable tagpolicy for someone with users_manage_tags privilege and admin role when tagpolicy is not set', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeWithTagPolicyPrivilegesUsersManageTagsRolesAdmin))
    const wrapper = mount(Users, {
      store,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    await flushPromises()
    const menu = wrapper.findAll('.moderation-dropdown-menu').at(0)
    const menu_items = menu.findAll('.el-dropdown-menu__item')
    const menu_items_text = menu_items.wrappers.map(menu_item => menu_item.text())

    expect(menu.isVisible()).toEqual(true)
    expect(menu_items_text.includes('users.disableAnySubscription')).toBe(true)
    expect(menu_items_text.includes('users.disableRemoteSubscription')).toBe(true)
    expect(menu_items_text.includes('users.forceNsfw')).toBe(true)
    expect(menu_items_text.includes('users.forceUnlisted')).toBe(true)
    expect(menu_items_text.includes('users.sandbox')).toBe(true)
    expect(menu_items_text.includes('users.stripMedia')).toBe(true)

    store.state.users.mrfPolicies = []
    await flushPromises()

    const menu_items_text_no_policy = menu.findAll('.el-dropdown-menu__item').wrappers.map(menu_item => menu_item.text())
    expect(menu_items_text_no_policy.includes('users.disableAnySubscription')).toBe(false)
    expect(menu_items_text_no_policy.includes('users.disableRemoteSubscription')).toBe(false)
    expect(menu_items_text_no_policy.includes('users.forceNsfw')).toBe(false)
    expect(menu_items_text_no_policy.includes('users.forceUnlisted')).toBe(false)
    expect(menu_items_text_no_policy.includes('users.sandbox')).toBe(false)
    expect(menu_items_text_no_policy.includes('users.stripMedia')).toBe(false)

    expect(menu_items_text_no_policy.includes('users.enableTagPolicy')).toBe(true)

    done()
  })

  it('shows for someone with users_manage_credentials privilege and shows proper entries', async (done) => {
    const store = new Vuex.Store(cloneDeep(storeWithPrivilegesUsersManageCredentialsNoRoles))
    const wrapper = mount(Users, {
      store,
      localVue,
      sync: false,
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
    await flushPromises()
    const menu = wrapper.findAll('.moderation-dropdown-menu').at(0)
    const menu_items = menu.findAll('.el-dropdown-menu__item')
    const menu_items_text = menu_items.wrappers.map(menu_item => menu_item.text()).sort()

    expect(menu.isVisible()).toEqual(true)
    expect(menu_items_text).toEqual(['users.getPasswordResetToken'])

    done()
  })
})
