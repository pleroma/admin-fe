// SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>
// SPDX-License-Identifier: AGPL-3.0-only

import app from '@/store/modules/app'
import settings from '@/store/modules/settings'
import user from '@/store/modules/user'
import userProfile from '@/store/modules/userProfile'
import users from '@/store/modules/users'
import getters from '@/store/getters'

export const storeConfig = {
  modules: {
    app,
    settings,
    user: {
      ...user,
      state: {
        ...user.state,
        roles: ['admin'],
        privileges: ['users_manage_activation_state', 'users_delete', 'users_manage_tags', 'users_manage_credentials']
      }
    },
    userProfile,
    users
  },
  getters
}

export const storeWithTagPolicy = {
  modules: {
    app,
    settings,
    user: {
      ...user,
      state: {
        ...user.state,
        roles: ['admin'],
        privileges: ['users_manage_activation_state', 'users_delete', 'users_manage_tags', 'users_manage_credentials']
      }
    },
    userProfile,
    users: {
      ...users,
      state: {
        ...users.state,
        mrfPolicies: ['Pleroma.Web.ActivityPub.MRF.TagPolicy']
      }
    }
  },
  getters
}

export const storeWithRoleAdminNoPrivileges = {
  modules: {
    app,
    settings,
    user: {
      ...user,
      state: {
        ...user.state,
        roles: ['admin'],
        privileges: []
      }
    },
    userProfile,
    users
  },
  getters
}


export const storeWithNoRolesNoPrivileges = {
  modules: {
    app,
    settings,
    user: {
      ...user,
      state: {
        ...user.state,
        roles: [],
        privileges: []
      }
    },
    userProfile,
    users
  },
  getters
}
