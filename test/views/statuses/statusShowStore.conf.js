// SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>
// SPDX-License-Identifier: AGPL-3.0-only

import app from '@/store/modules/app'
import peers from '@/store/modules/peers'
import user from '@/store/modules/user'
import userProfile from '@/store/modules/userProfile'
import users from '@/store/modules/users'
import settings from '@/store/modules/settings'
import status from '@/store/modules/status'
import getters from '@/store/getters'

export default {
  modules: {
    app,
    peers,
    settings,
    status,
    user: {
      ...user,
      state: {
        ...user.state,
        roles: ['admin'],
        privileges: [
          'users_manage_activation_state',
          'users_delete',
          'users_manage_tags',
          'users_manage_credentials',
          'messages_delete'
        ]
      }
    },
    userProfile,
    users
  },
  getters
}
