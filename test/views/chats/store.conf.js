// SPDX-FileCopyrightText: 2019-2022 Pleroma Authors <https://pleroma.social>
// SPDX-License-Identifier: AGPL-3.0-only

import app from '@/store/modules/app'
import settings from '@/store/modules/settings'
import user from '@/store/modules/user'
import chat from '@/store/modules/chat'
import getters from '@/store/getters'

export const storeConfig = {
  modules: {
    app,
    settings,
    user,
    chat
  },
  getters
}

export const storeWithTagPolicy = {
  modules: {
    app,
    settings,
    user,
    chat
  },
  getters
}
